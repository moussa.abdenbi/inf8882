# Projet INF8882

Ce dépôt contient les fichiers et scripts utilisaient dans le cadre du cours INF8882 pour un projet de session.
Un court manuel d'utilisation est aussi fournit.

## Dictionnaire et fichiers CSV

On dispose d'une version électronique [cide](dico/cide.dig) du dictionnaire *Cambridge International Dictionary of English* 
ainsi que des fichiers [CSV](https://en.wikipedia.org/wiki/Comma-separated_values) pour l'ingestion dans [Neo4j](https://neo4j.com/).

Ces fichiers CSV représentent ce dictionnaire sous un format de graphe dont les [noeuds](dico/cide_nodes.csv) et les [arcs](dico/cide_arcs.csv) 
sont respectivement les lexèmes et les relations de définition du dictionnaire.

## Implémentation

Les scripts python implémentant la [recherche tabou](scripts/recherche_tabou.py) et [l'algorithme glouton](scripts/algorithme_glouton.py) présentés dans 
le rapport du projet sont aussi fournit, ainsi que l'implémentation de fonction utilitaires pour les besoins des algorithmes.

L'appel à ces scripts se fait ainsi,
```sh
python -B algoithme_glouton.py path_to_graph
python -B recherche_tabou.py path_to_graph
```
où `path_to_graph` est une représentation en [liste d'adjacence](https://en.wikipedia.org/wiki/Adjacency_list) d'un graphe.